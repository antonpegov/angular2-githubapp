import { Injectable } from '@angular/core';
import { Init } from './init-todos';


@Injectable()
export class TodoService extends Init {

  constructor() {
    super(); // Вызов родительского конструктора
    //console.log('TodoService Initialized...');
    this.load();
  }
  getTodos(){
    let todos = JSON.parse(localStorage.getItem('todos'));
    return todos;
  }
  saveTodos(todos){
    localStorage.setItem('todos',JSON.stringify(todos));
  }
  addTodo(newTodo){
    let todos = this.getTodos();
    todos.push(newTodo);
    this.saveTodos(todos);
  }
  deleteTodo(index){
    let todos = this.getTodos();
    todos.splice(index,1);
    this.saveTodos(todos);
  }
  changeTodo(index,text){
    let todos = this.getTodos();
    todos[index].text = text;
    this.saveTodos(todos);
  }
  reset(){
    localStorage.removeItem('todos');
    this.load();
  }
}
