export class Init{
    load(){
        if(localStorage.getItem('todos') === null || localStorage.getItem('todos') === undefined){
            //console.log('No Todos Found, creating new');
            var todos = [
                {text: 'Сбегать на пробежку'},
                {text: 'Принять душ'},
                {text: 'Позавтракать'}
            ];
            localStorage.setItem('todos',JSON.stringify(todos));
            return;
        } else {
            //console.log('Found Todos...');
            return;
        }
    }
}