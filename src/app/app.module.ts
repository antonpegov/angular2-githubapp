import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { GithubComponent } from './github/github.component';
import { TodosComponent } from './todos/todos.component';
import { TodoService } from './services/todo.service';

@NgModule({
  declarations: [
    AppComponent,
    GithubComponent,
    TodosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [TodoService],
  bootstrap: [GithubComponent,TodosComponent]
})
export class AppModule { }
