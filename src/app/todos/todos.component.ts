import { Component, OnInit } from '@angular/core';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css','../../../node_modules/bootstrap/dist/css/bootstrap.min.css'],
})
export class TodosComponent implements OnInit {
  todos;
  text;
  editIndex;
  appState = 'defoult';
  constructor(private _todoService:TodoService) { 

  }

  ngOnInit() {
    this.todos = this._todoService.getTodos();
  }
  addTodo(){
    //console.log('Adding text: %s', this.text);
    if(this.text){
      let newTodo = {
        text: this.text
      }
      this.todos.push(newTodo);
      this._todoService.addTodo(newTodo);
      this.text = null;
    };
  }
  deleteTodo(index){
    if(this.appState === 'edited') return;
    //console.log('Index = %s, deleting %s',index , this.todos[index].text)
    this.todos.splice(index,1);
    this._todoService.deleteTodo(index);
  }
  editTodo(index){
    if (this.appState === 'edited') return;
    this.appState = 'edited'; 
    this.editIndex = index;
    this.text = this.todos[index].text;  
  }
  submitTodo(){
    this.todos[this.editIndex].text = this.text;
    this._todoService.changeTodo(this.editIndex, this.text)
    this.init();
  }
  cancelEdit(){
    this.init();
  }
  init(){
    this.text = null;
    this.appState = 'defoult';
    this.editIndex = null;
  }
  reset(){
    this._todoService.reset(); 
    this.todos = this._todoService.getTodos();
  }
}
